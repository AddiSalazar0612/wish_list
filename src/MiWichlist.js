import { html, css, LitElement } from 'lit-element';

export class MiWichlist extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--mi-wichlist-text-color, #000);
      }
      button{
        float: left;
      }
      ul{
        text-align: left;
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      wish: {type: String},
      wishes: {type: Array},
      counter: { type: Number },
      tecla: {type: Number},
      wi: {type: Array},
      word: {type: String},
      ver: {type: Boolean},
      ar: {type: String},
      nod: {type: String}
    };
  }

  constructor() {
    super();
    this.title = 'My WishList';
    this.wish= 'New wish';
    this.counter=0;
    this.ver=true;
    this.wi= [];
    this.addEventListener("keydown", function (event) {
      if(event.keyCode==13){this.word= this.shadowRoot.querySelector('#wi').value;
      this.wi.push(this.word);}
    }); 
  }

  __borrar(ar) {
    
  }

  
  

  render() {
    return html`
      <h2>${this.title} </h2>
      <fieldset>
      <legend>${this.wish}</legend>
      <input type="text" placeholder="Add a new wish" id="wi" keydown>
      </fieldset><br>
      <button @click=${this.__borrar}>Archive done</button>
      <br>
      <ul>
        ${this.wi.map(i => html`<li><input type="checkbox"  @change="${this.doChange}">${i}</li>`)}
      </ul>
    `;
  }

  doChange(e) {
    if(e.target.checked==true){
    console.log( "Cambiar activo a: ", e.target.checked );
    this.ver = e.target.checked;
    this.wi.pop();
    console.log(this.wi);
    this.ver=false;
    //return this.wi;
    }
  }
}
